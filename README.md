# Research

## Published

* [![DOI:10.1038/s41598-021-93783-8](https://zenodo.org/badge/DOI/10.1038/s41598-021-93783-8.svg)](https://doi.org/10.1038/s41598-021-93783-8)
[![Citation Badge](https://api.juleskreuer.eu/citation-badge.php?doi=10.1038/s41598-021-93783-8)](https://juleskreuer.eu/projekte/citation-badge/) [A Fuzzy Rank-based Ensemble of CNN Models for Classification of Cervical Cytology"](https://github.com/Rohit-Kundu/Fuzzy-Rank-Ensemble)
* [![DOI:10.3390/diagnostics12051173](https://zenodo.org/badge/DOI/10.3390/diagnostics12051173.svg)](https://doi.org/10.3390/diagnostics12051173)
[![Citation Badge](https://api.juleskreuer.eu/citation-badge.php?doi=10.3390/diagnostics12051173)](https://juleskreuer.eu/projekte/citation-badge/) [An Ensemble of CNN Models for Parkinson’s Disease Detection Using DaTscan Images](https://gitlab.com/digiratory/biomedimaging/parkinson-detector)
* [![DOI:10.1016/j.bspc.2022.104189](https://zenodo.org/badge/DOI/10.1016/j.bspc.2022.104189.svg)](https://doi.org/10.1016/j.bspc.2022.104189)
[![Citation Badge](https://api.juleskreuer.eu/citation-badge.php?doi=10.1016/j.bspc.2022.104189)](https://juleskreuer.eu/projekte/citation-badge/)
 [Segmentation of patchy areas in biomedical images based on local edge density estimation](https://gitlab.com/digiratory/biomedimaging/bcanalyzer)
 * [![DOI:10.3389/fninf.2023.1101112](https://zenodo.org/badge/DOI/10.3389/fninf.2023.1101112.svg)](https://doi.org/10.3389/fninf.2023.1101112)
[![Citation Badge](https://api.juleskreuer.eu/citation-badge.php?doi=10.3389/fninf.2023.1101112)](https://juleskreuer.eu/projekte/citation-badge/) [Video-based marker-free tracking and multi-scale analysis of mouse locomotor activity and behavioral aspects in an open field arena: A perspective approach to the quantification of complex gait disturbances associated with Alzheimer's disease](https://gitlab.com/digiratory/research/-/tree/main/Multiscale_analysis_of_AD_mouse_animal_behavior)
* [![DOI:10.1109/CTSYS.2017.8109568](https://zenodo.org/badge/DOI/10.1109/CTSYS.2017.8109568.svg)](https://doi.org/10.1109/CTSYS.2017.8109568)
[![Citation Badge](https://api.juleskreuer.eu/citation-badge.php?doi=10.1109/CTSYS.2017.8109568)](https://juleskreuer.eu/projekte/citation-badge/) [Research and implementation of the algorithm for data de-identification for Internet of Things](https://gitlab.com/digiratory/RAPPOR)
* [![DOI:10.1038/s41597-023-02065-7](https://zenodo.org/badge/DOI/10.1038/s41597-023-02065-7.svg)](https://doi.org/10.1038/s41597-023-02065-7)
[![Citation Badge](https://api.juleskreuer.eu/citation-badge.php?doi=10.1038/s41597-023-02065-7)](https://juleskreuer.eu/projekte/citation-badge/) [Brightfield vs Fluorescent Staining Dataset-A Test Bed Image Set for Machine Learning based Virtual Staining](https://gitlab.com/digiratory/biomedimaging/virtualstaining-dataset)
* [![DOI:10.5194/isprs-archives-XLVIII-2-W3-2023-233-2023](https://zenodo.org/badge/DOI/10.5194/isprs-archives-XLVIII-2-W3-2023-233-2023.svg)](https://doi.org/10.5194/isprs-archives-XLVIII-2-W3-2023-233-2023)
[![Citation Badge](https://api.juleskreuer.eu/citation-badge.php?doi=10.5194/isprs-archives-XLVIII-2-W3-2023-233-2023)](https://juleskreuer.eu/projekte/citation-badge/) [Python implementation for Slope Difference Distribution Segmentation](https://gitlab.com/digiratory/sdd-segmentation)


## Upcoming


